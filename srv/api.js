'use strict';

module.exports = function(app) {
  require('./apiModel.js')(app);
  require('./apiUser.js')(app);
  require('./apiPost.js')(app);
  require('./apiTopic.js')(app);
  require('./apiAuth.js')(app);
  require('./apiFilm.js')(app);
  require('./apiCatalog.js')(app);

  app.apiUser = new app.ApiUser();
  app.apiPost = new app.ApiPost();
  app.apiTopic = new app.ApiTopic();
  app.apiAuth = new app.ApiAuth();
  app.apiFilm = new app.ApiFilm();
  app.apiCatalog = new app.ApiCatalog();

};
