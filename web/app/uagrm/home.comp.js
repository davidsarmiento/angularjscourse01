/**
* Home Component
**/
(function() {
  'use strict';
  angular.module('app.uagrm')
  .component('viewUagrmHome', {
    templateUrl: templateUrl('app/uagrm/home.comp.html'),
    controller: HomeCtrl,
  });

  function HomeCtrl() {
    var $ctrl = this;
    $ctrl.ahora = new Date();
    $ctrl.den = 6523;
  }
})();
