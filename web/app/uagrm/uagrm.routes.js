/**
* Routes
**/
(function() {
  'use strict';
  angular.module('app.uagrm')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .abs('app.uagrm', '/univ')
    .when('app.uagrm.home', '/', {
      template: '<view-uagrm-home></view-uagrm-home>',
      title: 'UAGRM'
    })
    .when('app.uagrm.list', '/list', {
      template: '<view-uagrm-list></view-uagrm-list>',
      title: 'Listado'
    })
    .when('app.uagrm.view', '/view/:id', {
      template: '<view-uagrm-view topic="$resolve.topic" posts="$resolve.posts"></view-uagrm-view>',
      title: 'Categoria',
      resolve: {
        topic: topicResolve,
        posts: postsResolve
      }
    });
  }

  topicResolve.$inject = ['TopicService', '$stateParams'];
  function topicResolve(TopicService, $stateParams) {
    return TopicService.findById($stateParams.id);
  }

  postsResolve.$inject = ['$stateParams', 'PostService'];
  function postsResolve($stateParams, PostService) {
    return PostService.list($stateParams.id);
  }
})();
