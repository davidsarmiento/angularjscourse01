/**
* UAGRM Module
**/
(function() {
  'use strict';
  angular.module('app.uagrm', ['app.core']);
})();
