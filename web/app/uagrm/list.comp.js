/**
* List Component
**/
(function() {
  'use strict';
  angular.module('app.uagrm')
  .component('viewUagrmList', {
    templateUrl: templateUrl('app/uagrm/list.comp.html'),
    controller: ListCtrl
  });


  ListCtrl.$inject = ['TopicService'];
  function ListCtrl(TopicService) {
    var $ctrl = this;
    $ctrl.$onInit = $onInit;
    $ctrl.lista = [];
    $ctrl.selected = null;
    $ctrl.selectItem = selectItem;

    function $onInit() {
      TopicService.list()
      .then(function(list) {
        $ctrl.lista = list;
        console.log('lista', list);
      });
    }
    function selectItem(item) {
      $ctrl.selected = item.id;
    }
  }
})();
