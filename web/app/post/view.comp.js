/**
* Post View Component
**/
(function() {
  'use strict';
  angular.module('app.post')
  .component('viewPostView', {
    templateUrl: templateUrl('app/post/view.comp.html'),
    controller: ViewPostViewCtrl,
    bindings: {
      topic: '<',
      posts: '<'
    }
  });

  ViewPostViewCtrl.$inject = ['$rootScope'];
  function ViewPostViewCtrl($rootScope) {
    var $ctrl = this;
    $ctrl.searchText = '';
    $ctrl.$onInit = $onInit;

    function $onInit() {
      $rootScope.isLoadingRoute = false;
    }
  }
})();
