/**
* Post Routes
**/
(function() {
  'use strict';
  angular.module('app.post')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .abs('app.post', '/post')
    .when('app.post.view', '/:id/view', {
      template: '<view-post-view topic="$resolve.topic" posts="$resolve.posts"></view-post-view>',
      title: 'Mensajes',
      resolve: {
        topic: topicResolve,
        posts: postsResolve,
        startLoading: startLoading
      }
    })
    .when('app.post.new', '/:id/new', {
      templateUrl: templateUrl('app/post/new.tpl.html'),
      controller: 'PostNewCtrl as vm',
      title: 'Agregar Mensaje',
      resolve: {
        //topics: topicsResolve
      }
    });

    topicResolve.$inject = ['$stateParams', 'TopicService'];
    function topicResolve($stateParams, TopicService) {
      return TopicService.findById($stateParams.id);
    }

    postsResolve.$inject = ['$stateParams', 'PostService'];
    function postsResolve($stateParams, PostService) {
      return PostService.list($stateParams.id);
    }

    topicsResolve.$inject = ['TopicService'];
    function topicsResolve(TopicService) {
      return TopicService.list();
    }

    startLoading.$inject = ['$rootScope', '$timeout'];
    function startLoading($rootScope, $timeout) {
      $rootScope.isLoadingRoute = true;
      return $timeout(0);
    }
  }

})();
