/**
* Edit Post Directive
**/
(function() {
  'use strict';
  angular.module('app.post')
  .directive('editPost', editPostDirective);

  function editPostDirective() {
    return {
      restrict: 'E',
      templateUrl: templateUrl('app/post/editPost.tpl.html'),
      scope: {},
      controller: EditPostController,
      controllerAs: 'ctrl',
      bindToController: {
        model: '='
      }
    };
  }
  EditPostController.$inject = ['$scope'];
  function EditPostController($scope) {
    var ctrl = this;
  }
})();
