/**
* Core Module
**/
(function() {
  'use strict';
  angular.module('app', [
    'app.core',
    'app.directives',
    'app.layout',
    'app.topic',
    'app.post',
    'app.uagrm',
    'app.catalog'
  ]);
  angular.module('app.core', [
    'ngAnimate',
    'ngMessages',
    'ngSanitize',
    'ui.router',
    'ui.bootstrap'
  ]);
})();
