/**
* Template Cache
*/
(function() {
  'use strict';
  /*jshint devel:true */
  angular.module('app.core')
  .run(runTemplateCache);

  runTemplateCache.$inject = ['$templateCache', 'env'];
  function runTemplateCache($templateCache, env) {
    if (!env.logTemplaceCache) {return;}
    var fcnGet = $templateCache.get;
    $templateCache.get = function() {
      var args = [].slice.call(arguments);
      var res = fcnGet.apply($templateCache, args);
      console.info('$templateCache', {args:args[0], res:res});
      return res;
    };
  }
})();
