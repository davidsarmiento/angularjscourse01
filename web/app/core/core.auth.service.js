/**
* Auth Service
**/
(function() {
  'use strict';
  angular.module('app.core')
  .service('AuthService', AuthService);

  AuthService.$inject = ['$http', 'env'];
  function AuthService($http, env) {
    BaseService.call(this, $http, env, 'auth');
    this.auth = this.user = null;
  }
  utils.inherits(AuthService, BaseService);
  var pAuth = AuthService.prototype;
  pAuth.me = function() {
    var self = this;
    return this.$http.get(this.url('me')).then(this.dataData)
    .then(function(user) {
      self.auth = self.user = user;
      return user;
    });
  };
  pAuth.pem = function() {
    return this.$http.get(this.url('pem')).then(this.dataData);
  };
  pAuth.rsa = function() {
    /* globals JSEncrypt */
    return this.pem()
    .then(function(data) {
      var rsa = new JSEncrypt();
      rsa.setPublicKey(data.key);
      return rsa;
    });
  };
  pAuth.login = function(user, pass) {
    var self = this;
    return this.rsa()
    .then(function(rsa) {
      return self.$http.post(self.url('login'), {
        username: user,
        password: rsa.encrypt(pass)
      });
    }).then(self.dataData)
    .then(function(user) {
      self.auth = self.user = user;
      return user;
    });
  };
  pAuth.logoutUrl = function() {
    return this.url('logout');
  };
})();
