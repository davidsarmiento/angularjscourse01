/**
* Topic List View Component
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .component('viewTopicList', {
    templateUrl: templateUrl('app/topic/list.comp.html'),
    controller: ViewTopicListCtrl,
    bindings: {
      topics: '<'
    }
  });

  ViewTopicListCtrl.$inject = ['$rootScope'];
  function ViewTopicListCtrl($rootScope) {
    var $ctrl = this;
    $ctrl.$onInit = $onInit;

    function $onInit() {
      console.log({$ctrl:$ctrl,$rootScope:$rootScope});
      $rootScope.loadedRoute();
    }
  }
})();
