/**
* Topic Module
**/
(function() {
  'use strict';
  angular.module('app.topic', ['app.core']);
})();
