/**
* Topic Service
**/
(function() {
  'use strict';
  angular.module('app.topic')
  .service('TopicService', TopicService);

  TopicService.$inject = ['$http', 'env'];
  function TopicService($http, env) {
    ModelService.call(this, $http, env, 'topic');
  }
  utils.inherits(TopicService, ModelService);
})();
