/**
* Catalog Service
**/
(function() {
  'use strict';
  angular.module('app.catalog')
  .service('CatalogService', CatalogService)
  .constant('CategoryList', [
    'Libros - Ciencia Ficcion',
    'Libros - Tecnicos',
    'Libros - Historia',
    'Musica - Rock',
    'Musica - Clasica',
    'Musica - Instrumental',
    'Videos - Series',
    'Videos - Novelas',
    'Videos - YouTube'
  ]);

  CatalogService.$inject = ['$http', 'env'];
  function CatalogService($http, env) {
    ModelService.call(this, $http, env, 'catalog');
  }
  utils.inherits(CatalogService, ModelService);
  var sProto = CatalogService.prototype;
  sProto.search = function(title, category, author, description) {
    var params = {
      title: title,
      category: !!category && category !== '*' ? category : null,
      author: author,
      description: description
    };
    return this.$http.get(this.url(), {params:params}).then(this.dataData);
  };

})();
