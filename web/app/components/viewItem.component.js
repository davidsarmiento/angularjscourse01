/**
* View Item Compnent
**/
(function() {
  'use strict';
  angular.module('app.components')
  .component('viewItem', {
    templateUrl: templateUrl('app/components/viewItem.component.html'),
    controller: ViewItemCtrl,
    bindings: {
      post: '<',
      onLike: '&'
    }
  });

  function ViewItemCtrl() {
    var $ctrl = this;
    $ctrl.like = like;

    function like() {
      $ctrl.onLike({post:$ctrl.post});
    }
  }
})();
