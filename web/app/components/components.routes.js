/**
* Component Routes
**/
(function() {
  'use strict';
  angular.module('app.components')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .abs('app.components', '/components')
    .when('app.components.view', '/:id/view', {
      template: '<component-view topic="$resolve.topic" posts="$resolve.posts"></component-view>',
      title: 'Componentes Lista',
      resolve: {
        topic: topicResolve,
        posts: postsResolve
      }
    });
    topicResolve.$inject = ['$stateParams', 'TopicService'];
    function topicResolve($stateParams, TopicService) {
      return TopicService.findById($stateParams.id);
    }
    postsResolve.$inject = ['$stateParams', 'PostService'];
    function postsResolve($stateParams, PostService) {
      return PostService.list($stateParams.id);
    }
  }
})();
