/**
* View List Component
**/
(function() {
  'use strict';
  angular.module('app.components')
  .component('viewList', {
    templateUrl: templateUrl('app/components/viewList.component.html'),
    controller: ViewListCtrl,
    bindings: {
      posts: '<'
    }
  });

  ViewListCtrl.$inject = ['$rootScope', 'PostService'];
  function ViewListCtrl($rootScope, PostService) {
    var $ctrl = this;
    $ctrl.searchText = '';
    $ctrl.like = like;
    $ctrl.onSearchText = onSearchText;

    function like(post) {
      /* jshint devel:true */
      console.log({like:post});
      post.likes += 1;
      PostService.update(post)
      .then(function(data) {
      }, $rootScope.fcnError('Post Like'));
    }
    function onSearchText(search) {
      $ctrl.searchText = search;
    }
  }
})();
