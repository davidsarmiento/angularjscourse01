/**
* View Component
**/
(function() {
  'use strict';
  angular.module('app.components')
  .component('componentView', {
    templateUrl: templateUrl('app/components/view.component.html'),
    controller: ComponentViewCtrl,
    bindings: {
      topic: '<',
      posts: '<'
    }
  });

  ComponentViewCtrl.$inject = [];
  function ComponentViewCtrl() {
    var $ctrl = this;
  }
})();
