/**
* App Bar Component
**/
(function() {
  'use strict';
  angular.module('app.directives')
  .component('myAppBar', {
    templateUrl: templateUrl('app/directives/appbar.tpl.html'),
    controller: MyAppBarCtrl,
    controllerAs: 'ctrl'
  });

  MyAppBarCtrl.$inject = ['$state', '$location', '$rootScope',
    'env', 'AuthService'];
  function MyAppBarCtrl($state, $location, $rootScope, env, AuthService) {
    var ctrl = this;
    ctrl.$root = $rootScope;
    ctrl.menus = [];
    ctrl.appImage = env.webUrl + basePath + 'img/favicon/android-chrome-192x192.png';
    ctrl.logoutUrl = AuthService.logoutUrl();
    ctrl.pageTitle = null;
    ctrl.auth = AuthService;
    ctrl.$onInit = $onInit;
    ctrl.$onDestroy = $onDestroy;
    $rootScope.$on('auth-changed', authChanged);
    $rootScope.$on('$stateChangeSuccess', stateChangeSuccess);

    function $onInit() {
      //console.log('$onInit', {ctrl:ctrl});
    }
    function $onDestroy() {
      $rootScope.$off('auth-changed', authChanged);
      $rootScope.$off('$stateChangeSuccess', stateChangeSuccess);
    }
    function authChanged(e, user) {
      updateMenus();
    }
    function stateChangeSuccess(event, toState, toParams, fromState, fromParams) {
      updateMenus();
    }
    function updateMenus() {
      var menus = [];
      addMenu(menus, 'Categorías', 'app.topic.list');
      addMenu(menus, 'Fútbol', 'app.post.view', {id:1});
      addMenu(menus, 'Populares', 'app.topic.populares');
      ctrl.menus = menus;
    }
    function addMenu(menus, text, state, params) {
      if (!ctrl.auth.user) {return;}
      var url = $state.href(state, params).substr(2);
      var active = url === $location.path();
      var sParams = params ? ['(', JSON.stringify(params),  ')'].join('') : '';
      menus.push({
        text: text,
        url: url,
        state: state + sParams,
        active: active
      });
    }
  }
})();
