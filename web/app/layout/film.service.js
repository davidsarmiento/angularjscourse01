/**
* Film Service
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .service('FilmService', FilmService);

  FilmService.$inject = ['$http', 'env'];
  function FilmService($http, env) {
    ModelService.call(this, $http, env, 'film');
  }
  utils.inherits(FilmService, ModelService);

})();
