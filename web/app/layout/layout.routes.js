/**
* Routes
**/
(function() {
  'use strict';
  angular.module('app.core')
  .config(configRoutes);

  configRoutes.$inject = ['stateProvider'];
  function configRoutes(stateProvider) {
    stateProvider
    .else('/ui/')
    .abs('app', '/ui')
    .when('app.home', '/', {
      template: '<view-home topics="$resolve.topics"></view-home>',
      title: 'Bienvenido',
      resolve: {
        topics: resolveTopics
      }
    })
    .when('app.login', '/login', {
      template: '<view-login></view-login>',
      title: 'Ingresar'
    })
    .abs('app.film', '/film')
    .when('app.film.list', '/', {
      template: '<view-film films="$resolve.films"></view-film>',
      title: 'Films',
      resolve: {
        films: filmsResolve
      }
    })
    .when('app.film.edit', '/:id', {
      template: '<view-film-edit film="$resolve.film"></view-film-edit>',
      title: 'Edit Film',
      resolve: {
        film: filmResolve
      }
    });
  }

  resolveTopics.$inject = ['TopicService'];
  function resolveTopics(TopicService) {
    return TopicService.list();
  }

  filmsResolve.$inject = ['FilmService'];
  function filmsResolve(FilmService) {
    return FilmService.list();
  }
  filmResolve.$inject = ['FilmService', '$stateParams'];
  function filmResolve(FilmService, $stateParams) {
    return FilmService.findById($stateParams.id);
  }
})();
