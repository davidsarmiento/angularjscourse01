/**
* Marked filter
**/
(function() {
  'use strict';
  angular.module('app.layout')
  .filter('marked', markedFilter);

  function markedFilter() {
    /* globals marked */
    /* jshint devel: true */
    return function markedFilterImpl(input) {
      if (!input) {return '';}
      try {
        return marked(input);
      } catch(e) {
        console.error(e);
        return '';
      }
    };
  }
})();
