/**
* Utils
**/
(function() {
  'use strict';
  window.utils = {
    extend: function(target, sources) {
    	if (!Array.isArray(sources)) {
    		sources = [].slice.call(arguments, 1);
    	}
    	if (!target) { target = {}; }
    	sources.forEach(function(source) {
    		Object.keys(source).forEach(function(key) {
    			target[key] = source[key];
    		});
    	});
    	return target;
    },
    inherits: function(Child, Parent) {
    	Child.prototype = Object.create(Parent.prototype);
    	Child.prototype.constructor = Child;
    },
    findByProp: function(list, name, value) {
      for(var k=0,n=list.length; k<n; k++) {
        if (list[k][name] === value) {
          return list[k];
        }
      }
      return null;
    },
    indexOfProp: function(list, name, value) {
      for(var k=0,n=list.length; k<n; k++) {
        if (list[k][name] === value) {
          return k;
        }
      }
      return -1;
    },
    roundMoney: function(value) {
      return Math.round((value + 0.00001)*100) / 100;
    },
    range: function(min, max) {
      var arr = new Array(max-min+1);
      var c = arr.length;
      while(c--) {
        arr[c] = max--;
      }
      return arr;
    },
    isNullOrEmpty: function(str) {
      return str === null || str === undefined || str === '';
    },
    formatObj: function(frm, obj) {
      var self = this;
    	var res = frm;
      var pattern = /\$\{([a-z0-9_-]+)\}/gi;
      return frm.replace(pattern, function(m,p) {
        if (obj.hasOwnProperty(p)) {
          var val = obj[p];
          if (self.isNullOrEmpty(val)) {
            return m;
          } else {
            return val.toString();
          }
        }
        return m;
      });
    },
    className: function(name) {
      return name
      .replace(/([a-z])([A-Z])/g, '$1-$2').toLowerCase();
    },
    classNames: function(args) {
      /* jshint maxcomplexity: 30 */
      var self = this;
      var hasOwn = {}.hasOwnProperty;
  		var classes = [];
      if (!Array.isArray(args)) {
        args = [].slice.call(arguments);
      }

  		for (var i = 0; i < args.length; i++) {
  			var arg = args[i];
  			if (!arg) {continue;}

  			var argType = typeof arg;

  			if (argType === 'string') {
          classes.push(self.className(arg));
        } else if (argType === 'number') {
  				classes.push(arg);
  			} else if (Array.isArray(arg)) {
  				classes.push(self.classNames(arg));
  			} else if (argType === 'object') {
  				for (var key in arg) {
  					if (hasOwn.call(arg, key) && arg[key]) {
  						classes.push(self.className(key));
  					}
  				}
  			}
  		}
  		return classes.join(' ');
  	}
  };
})();
